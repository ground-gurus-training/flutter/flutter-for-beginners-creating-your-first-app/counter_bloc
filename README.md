# Counter App using Flutter Bloc

A simple counter app that uses Flutter Bloc for state management

## Flutter Bloc Package

[https://pub.dev/packages/flutter_bloc](https://pub.dev/packages/flutter_bloc)

[![Alt text](https://groundgurus-assets.s3.ap-southeast-1.amazonaws.com/Ground+Gurus+-+Logo+Small.png)](https://www.facebook.com/groups/1290693181003142)
